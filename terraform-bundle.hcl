terraform {
  # Version of Terraform to include in the bundle. An exact version number
  # is required.
  version = "1.5.7"
}

# Define which provider plugins are to be included
providers {
  # Include the newest "aws" provider version in the 1.0 series.
  # the bundle archive.
  nsxt = {
    source = "vmware/nsxt"
    versions = ["3.3.2"]
  }
  dns = {
    source = "hashicorp/dns"
    versions = ["3.3.2"]
  }
  null = {
    source = "hashicorp/null"
    versions = ["3.2.1"]
  }
  tls = {
      source = "hashicorp/tls"
      versions = ["4.0.4"]
  }
  local = {
      source = "hashicorp/local"
      versions = ["2.4.0"]
  }
  time = {
    source = "hashicorp/time"
    versions = ["0.9.1"]
  }
  harbor = {
    source = "goharbor/harbor"
    versions = ["3.10.3"]
  }
  vsphere = {
    source = "hashicorp/vsphere"
    versions = ["~>2.4.3"]
  }
  rancher2 = {
    source = "rancher/rancher2"
    versions = ["3.1.1"]
  }
  netbox = {
    source = "e-breuninger/netbox"
    versions = ["3.7.1", "2.0.7", "3.5.5"]
  }
  kubernetes = {
    source = "hashicorp/kubernetes"
    versions = ["2.23.0"]
  }
  helm = {
    source = "hashicorp/helm"
    versions = ["2.11.0"]
  }
  harvester = {
    source = "harvester/harvester"
    versions = ["0.6.3"]
  }
  random = {
    source = "hashicorp/random"
    versions = ["3.5.1"]
  }
  http = {
    source = "hashicorp/http"
    versions = ["3.4.0"]
  }
  libvirt = {
      source = "dmacvicar/libvirt"
      versions = ["0.7.1"]
  }
}





